package soc;

public class Mod_T {
    boolean Ext;
    public Asp[] ArAsp = new Asp[4];

    public Mod_T(boolean isExt) {
        Ext = isExt;
        ArAsp[0] = new Asp();
        ArAsp[1] = new Asp();
        ArAsp[2] = new Asp();
        ArAsp[3] = new Asp();
        if (Ext) {
            ArAsp[0].IntIsHF = true;
            ArAsp[0].Inf = new Inform();
            ArAsp[0].ExIsHF = false;

            ArAsp[1].IntIsHF = true;
            ArAsp[1].ExIsHF = true;
            ArAsp[1].Inf = new Inform();

            ArAsp[2].IntIsHF = false;
            ArAsp[2].ExIsHF = false;
            ArAsp[2].Inf = new Inform();

            ArAsp[3].IntIsHF = false;
            ArAsp[3].ExIsHF = true;
            ArAsp[3].Inf = new Inform();
        }
        if (Ext = false) {
            ArAsp[0].IntIsHF = false;
            ArAsp[0].ExIsHF = true;
            ArAsp[1].IntIsHF = false;
            ArAsp[1].ExIsHF = false;
            ArAsp[2].IntIsHF = true;
            ArAsp[2].ExIsHF = true;
            ArAsp[3].IntIsHF = true;
            ArAsp[3].ExIsHF = false;
        }

    }
}
